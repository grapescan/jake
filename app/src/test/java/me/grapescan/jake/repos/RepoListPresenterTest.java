package me.grapescan.jake.repos;

import android.support.annotation.NonNull;

import junit.framework.Assert;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.internal.verification.AtLeast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import me.grapescan.jake.BaseCard;
import me.grapescan.jake.TimeFormatterMessages;
import me.grapescan.jake.UiHandler;
import me.grapescan.jake.model.LocalCache;
import me.grapescan.jake.model.PagedDataSource;
import me.grapescan.jake.model.Repo;
import me.grapescan.jake.model.RepoRepository;
import me.grapescan.jake.repos.format.counter.CounterFormatterImpl;
import me.grapescan.jake.repos.format.time.RelativeTimeFormatterImpl;

import static org.mockito.Mockito.*;

public class RepoListPresenterTest {
    private static final UiHandler uiHandler = new UiHandler() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        @Override
        public void runOnUiThread(Runnable task) {
            executorService.submit(task);
        }
    };

    private static class InMemoryCache implements LocalCache<List<Repo>> {
        private List<Repo> data = new ArrayList<>();

        @Override
        public void save(@NonNull List<Repo> data) {
            this.data = data;
        }

        @Override
        public void clear() {
            data.clear();
        }

        @Override
        public void load(Callback<List<Repo>> callback) {
            callback.onDataLoaded(data);
        }
    }

    private static class FakeDataSource implements PagedDataSource<List<Repo>> {
        private static final int PAGE_SIZE = 10;

        @Override
        public void loadNextPage(int offset, Callback<List<Repo>> callback) {
            List<Repo> repos = new ArrayList<>(offset + PAGE_SIZE);
            for (int i = 0; i < offset + PAGE_SIZE; i++) {
                repos.add(new Repo(i, "", "", "", "", "", "", 0, 0, 0, new Date()));
            }
            callback.onDataLoaded(repos);
        }
    }

    private static class BrokenDataSource implements PagedDataSource<List<Repo>> {
        @Override
        public void loadNextPage(int offset, Callback<List<Repo>> callback) {
            callback.onError(new Exception("unknown error"));
        }
    }

    private final RepoRepository workingRepository = new RepoRepository(new InMemoryCache(), new FakeDataSource());
    private final RepoRepository brokenRepository = new RepoRepository(new InMemoryCache(), new BrokenDataSource());

    private MessageProvider messages = new MessageProvider() {
        @Override
        public String genericLoadingError() {
            return "Loading failed";
        }

        @Override
        public String noNetworkError() {
            return "No network";
        }

        @Override
        public String updatedOnTemplate() {
            return "Updated %s ago";
        }
    };

    private final Class cardListClass = (Class<List<BaseCard>>)(Class)ArrayList.class;

    private static void await() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInitialLoading() {
        RepoListContract.Presenter presenter = new RepoListPresenter(uiHandler, workingRepository, messages,
                new CounterFormatterImpl(), new RelativeTimeFormatterImpl(new TimeFormatterMessages()));
        RepoListContract.View view = mock(RepoListContract.View.class);
        presenter.onAttach(view);

        await();

        verify(view, times(1)).showProgress();
        verify(view, times(1)).stopRefreshing();
        verify(view, times(1)).showList(Matchers.<List<BaseCard>>any());
    }

    @Test
    public void testLoadingMore() {
        RepoListContract.Presenter presenter = new RepoListPresenter(uiHandler, workingRepository, messages,
                new CounterFormatterImpl(), new RelativeTimeFormatterImpl(new TimeFormatterMessages()));
        RepoListContract.View view = mock(RepoListContract.View.class);
        presenter.onAttach(view);

        await();

        ArgumentCaptor<List<BaseCard>> captor = ArgumentCaptor.forClass(cardListClass);
        verify(view, times(1)).showProgress();
        verify(view, times(1)).stopRefreshing();
        verify(view, times(1)).showList(captor.capture());

        int onePageSize = captor.getValue().size();
        for (int i = 0; i < onePageSize; i++) {
            presenter.onItemDisplayed(i);
        }

        await();

        verify(view, new AtLeast(1)).showList(captor.capture());
        int twoPagesSize = captor.getValue().size();

        Assert.assertTrue(twoPagesSize > onePageSize);
    }

    @Test
    public void testLoadingFailed() {
        RepoListContract.Presenter presenter = new RepoListPresenter(uiHandler, brokenRepository, messages,
                new CounterFormatterImpl(), new RelativeTimeFormatterImpl(new TimeFormatterMessages()));
        RepoListContract.View view = mock(RepoListContract.View.class);
        presenter.onAttach(view);

        await();

        verify(view, times(1)).showProgress();
        verify(view, times(1)).stopRefreshing();
        verify(view, times(1)).showError((String) any());
    }
}
