package me.grapescan.jake.model;

import android.support.annotation.NonNull;

import junit.framework.Assert;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

public class RepoRepositoryTest {
    private final PagedDataSource<List<Repo>> emptySource = new PagedDataSource<List<Repo>>() {
        @Override
        public void loadNextPage(int offset, Callback<List<Repo>> callback) {
            callback.onDataLoaded(Collections.<Repo>emptyList());
        }
    };

    private final PagedDataSource<List<Repo>> brokenSource = new PagedDataSource<List<Repo>>() {
        @Override
        public void loadNextPage(int offset, Callback<List<Repo>> callback) {
            callback.onError(new Exception("fake error"));
        }
    };

    private final PagedDataSource<List<Repo>> fiveReposSource = new PagedDataSource<List<Repo>>() {
        private final List<Repo> DATA = new ArrayList<Repo>() {{
            add(new Repo(0, "", "", "", "", "", "", 0, 0, 0, new Date()));
            add(new Repo(1, "", "", "", "", "", "", 0, 0, 0, new Date()));
            add(new Repo(2, "", "", "", "", "", "", 0, 0, 0, new Date()));
            add(new Repo(3, "", "", "", "", "", "", 0, 0, 0, new Date()));
            add(new Repo(4, "", "", "", "", "", "", 0, 0, 0, new Date()));
        }};

        @Override
        public void loadNextPage(final int offset, Callback<List<Repo>> callback) {
            callback.onDataLoaded(offset < DATA.size() ? new ArrayList<Repo>() {{ add(DATA.get(offset + 1)); }} : Collections.<Repo>emptyList());
        }
    };

    private final Class listClass = (Class<List<Repo>>)(Class)ArrayList.class;

    private LocalCache<List<Repo>> buildCache() {
        return new LocalCache<List<Repo>>() {
            private List<Repo> data = new ArrayList<>();

            @Override
            public void save(@NonNull List<Repo> data) {
                this.data = data;
            }

            @Override
            public void clear() {
                data.clear();
            }

            @Override
            public void load(Callback<List<Repo>> callback) {
                callback.onDataLoaded(data);
            }
        };
    }

    @Test
    public void testInitiallyEmpty() {
        RepoRepository repo = new RepoRepository(buildCache(), emptySource);
        RepoRepository.Callback callback = mock(RepoRepository.Callback.class);
        repo.getRepos(callback);
        ArgumentCaptor<List<Repo>> captor = ArgumentCaptor.forClass(listClass);
        verify(callback, only()).onDataLoaded(captor.capture());
        Assert.assertTrue(captor.getValue().size() == 0);
    }

    @Test
    public void testHandlesErrors() {
        RepoRepository repo = new RepoRepository(buildCache(), brokenSource);
        RepoRepository.Callback callback = mock(RepoRepository.Callback.class);
        repo.getRepos(callback);
        verify(callback, only()).onError((Throwable) any());
    }

    @Test
    public void testPagedLoading() {
        RepoRepository repo = new RepoRepository(buildCache(), fiveReposSource);
        RepoRepository.Callback callback = mock(RepoRepository.Callback.class);
        repo.getRepos(callback);
        repo.loadMore(callback);
        ArgumentCaptor<List<Repo>> captor = ArgumentCaptor.forClass(listClass);
        verify(callback, times(2)).onDataLoaded(captor.capture());
        Assert.assertTrue(captor.getValue().size() == 2);
    }

    @Test
    public void testDataReset() {
        RepoRepository repo = new RepoRepository(buildCache(), fiveReposSource);
        RepoRepository.Callback callback = mock(RepoRepository.Callback.class);
        repo.getRepos(callback);
        repo.loadMore(callback);
        repo.loadMore(callback);
        repo.getFreshRepos(callback);
        ArgumentCaptor<List<Repo>> captor = ArgumentCaptor.forClass(listClass);
        verify(callback, times(4)).onDataLoaded(captor.capture());
        Assert.assertTrue(captor.getValue().size() == 1);
    }
}
