package me.grapescan.jake;

import org.junit.Assert;
import org.junit.Test;

import me.grapescan.jake.repos.format.counter.CounterFormatterImpl;

public class CounterFormatterTest {
    private CounterFormatterImpl formatter = new CounterFormatterImpl();

    @Test
    public void testZeroInput() {
        Assert.assertEquals(formatter.format(0), "0");
    }

    @Test
    public void testNegativeInput() {
        Assert.assertEquals(formatter.format(-1), "-1");
    }

    @Test
    public void testHundredsInput() {
        Assert.assertEquals(formatter.format(100), "100");
        Assert.assertEquals(formatter.format(527), "527");
        Assert.assertEquals(formatter.format(999), "999");
    }

    @Test
    public void testThousandsInput() {
        Assert.assertEquals(formatter.format(1000), "1k");
        Assert.assertEquals(formatter.format(5274), "5.2k");
        Assert.assertEquals(formatter.format(9999), "9.9k");
    }

    @Test
    public void testMillionsInput() {
        Assert.assertEquals(formatter.format(1_000_000), "1M");
        Assert.assertEquals(formatter.format(5_274_314), "5.2M");
        Assert.assertEquals(formatter.format(9_999_999), "9.9M");
    }

    @Test
    public void testBillionsInput() {
        Assert.assertEquals(formatter.format(1_000_000_000L), "1G");
        Assert.assertEquals(formatter.format(5_274_314_193L), "5.2G");
        Assert.assertEquals(formatter.format(9_999_999_999L), "9.9G");
    }

    @Test
    public void testTrillionsInput() {
        Assert.assertEquals(formatter.format(1_000_000_000_000L), "1T");
        Assert.assertEquals(formatter.format(5_274_314_193_663L), "5.2T");
        Assert.assertEquals(formatter.format(9_999_999_999_999L), "9.9T");
    }

    @Test
    public void testQuadrillionsInput() {
        Assert.assertEquals(formatter.format(1_000_000_000_000_000L), "1P");
        Assert.assertEquals(formatter.format(5_274_314_193_663_412L), "5.2P");
        Assert.assertEquals(formatter.format(9_999_999_999_999_999L), "9.9P");
    }

    @Test
    public void testQuintillionsInput() {
        Assert.assertEquals(formatter.format(1_000_000_000_000_000_000L), "1E");
        Assert.assertEquals(formatter.format(5_274_314_193_663_412_413L), "5.2E");
    }
}
