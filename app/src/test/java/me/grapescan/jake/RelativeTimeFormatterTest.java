package me.grapescan.jake;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import me.grapescan.jake.repos.format.time.RelativeTimeFormatter;
import me.grapescan.jake.repos.format.time.RelativeTimeFormatterImpl;

public class RelativeTimeFormatterTest {
    private final static long WEEK_MILLIS = TimeUnit.DAYS.toMillis(7);
    private final static RelativeTimeFormatter formatter = new RelativeTimeFormatterImpl(new TimeFormatterMessages());
    private final long startTime = System.currentTimeMillis();

    private void batchTest(String expected, long... intervals) {
        for (long interval : intervals) {
            Assert.assertEquals(formatter.format(new Date(startTime), new Date(startTime - interval)), expected);
        }
    }

    @Test
    public void testSecondAgo() {
        batchTest("a second ago", 1000, 1270, 1999);
    }

    @Test
    public void testTwoSecondsAgo() {
        batchTest("2 seconds ago", 2000, 2782, 2999);
    }

    @Test
    public void testAlmostMinuteAgo() {
        batchTest("59 seconds ago", 59000);
    }

    @Test
    public void testMinuteAgo() {
        batchTest("a minute ago",
                TimeUnit.MINUTES.toMillis(1),
                TimeUnit.MINUTES.toMillis(1) + TimeUnit.SECONDS.toMillis(28),
                TimeUnit.MINUTES.toMillis(1) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testFiveMinutesAgo() {
        batchTest("5 minutes ago",
                TimeUnit.MINUTES.toMillis(5),
                TimeUnit.MINUTES.toMillis(5) + TimeUnit.SECONDS.toMillis(11),
                TimeUnit.MINUTES.toMillis(5) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testAlmostHourAgo() {
        batchTest("59 minutes ago",
                TimeUnit.MINUTES.toMillis(59),
                TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(43),
                TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testHourAgo() {
        batchTest("an hour ago",
                TimeUnit.HOURS.toMillis(1),
                TimeUnit.HOURS.toMillis(1) + TimeUnit.MINUTES.toMillis(2),
                TimeUnit.HOURS.toMillis(1) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testEightHourAgo() {
        batchTest("8 hours ago",
                TimeUnit.HOURS.toMillis(8),
                TimeUnit.HOURS.toMillis(8) + TimeUnit.MINUTES.toMillis(6),
                TimeUnit.HOURS.toMillis(8) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testAlmostDayAgo() {
        batchTest("23 hours ago",
                TimeUnit.HOURS.toMillis(23),
                TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(18),
                TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testDayAgo() {
        batchTest("a day ago",
                TimeUnit.DAYS.toMillis(1),
                TimeUnit.DAYS.toMillis(1) + TimeUnit.HOURS.toMillis(18) + TimeUnit.MINUTES.toMillis(13),
                TimeUnit.DAYS.toMillis(1) + TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testThreeDaysAgo() {
        batchTest("3 days ago",
                TimeUnit.DAYS.toMillis(3),
                TimeUnit.DAYS.toMillis(3) + TimeUnit.HOURS.toMillis(8) + TimeUnit.MINUTES.toMillis(1),
                TimeUnit.DAYS.toMillis(3) + TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testAlmostWeekAgo() {
        batchTest("6 days ago",
                TimeUnit.DAYS.toMillis(6),
                TimeUnit.DAYS.toMillis(6) + TimeUnit.MINUTES.toMillis(1),
                TimeUnit.DAYS.toMillis(6) + TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testWeekAgo() {
        batchTest("a week ago",
                TimeUnit.DAYS.toMillis(7),
                TimeUnit.DAYS.toMillis(10) + TimeUnit.HOURS.toMillis(7),
                TimeUnit.DAYS.toMillis(13) + TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testTwoWeeksAgo() {
        batchTest("2 weeks ago",
                TimeUnit.DAYS.toMillis(14),
                TimeUnit.DAYS.toMillis(17) + TimeUnit.MINUTES.toMillis(35),
                TimeUnit.DAYS.toMillis(20) + TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testAlmostMonthAgo() {
        batchTest("3 weeks ago",
                WEEK_MILLIS * 3,
                WEEK_MILLIS * 3 + TimeUnit.DAYS.toMillis(4) + TimeUnit.SECONDS.toMillis(3),
                WEEK_MILLIS * 3 + TimeUnit.DAYS.toMillis(6) + TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }

    @Test
    public void testMonthAgo() {
        batchTest("a month ago",
                WEEK_MILLIS * 4,
                WEEK_MILLIS * 6 + TimeUnit.DAYS.toMillis(4) + TimeUnit.SECONDS.toMillis(3),
                WEEK_MILLIS * 7 + TimeUnit.DAYS.toMillis(6) + TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(59) + TimeUnit.SECONDS.toMillis(59)
        );
    }
}
