package me.grapescan.jake;

import java.util.Locale;

import me.grapescan.jake.repos.format.time.MessageProvider;

public class TimeFormatterMessages implements MessageProvider {
    @Override
    public String seconds(int n) {
        return n == 1 ? "a second ago" : String.format(Locale.ROOT, "%d seconds ago", n);
    }

    @Override
    public String minutes(int n) {
        return n == 1 ? "a minute ago" : String.format(Locale.ROOT, "%d minutes ago", n);
    }

    @Override
    public String hours(int n) {
        return n == 1 ? "an hour ago" : String.format(Locale.ROOT, "%d hours ago", n);
    }

    @Override
    public String days(int n) {
        return n == 1 ? "a day ago" : String.format(Locale.ROOT, "%d days ago", n);
    }

    @Override
    public String weeks(int n) {
        return n == 1 ? "a week ago" : String.format(Locale.ROOT, "%d weeks ago", n);
    }

    @Override
    public String months(int n) {
        return n == 1 ? "a month ago" : String.format(Locale.ROOT, "%d months ago", n);
    }

    @Override
    public String years(int n) {
        return n == 1 ? "a year ago" : String.format(Locale.ROOT, "%d year ago", n);
    }
}
