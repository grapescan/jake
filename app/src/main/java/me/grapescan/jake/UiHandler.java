package me.grapescan.jake;

public interface UiHandler {
    void runOnUiThread(Runnable task);
}
