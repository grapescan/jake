package me.grapescan.jake.model;


public interface DataSource<T> {
    interface Callback<T> {
        void onDataLoaded(T data);
        void onError(Throwable error);
    }

    void load(Callback<T> callback);
}
