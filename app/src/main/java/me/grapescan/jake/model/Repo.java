package me.grapescan.jake.model;

import android.support.annotation.Nullable;

import java.util.Date;

public class Repo {
    public final long id;
    public final String name;
    public final String fullName;
    public final String description;
    public final String homepage;
    public final String language;
    public final String webUrl;
    public final long forksCount;
    public final long watchersCount;
    public final long starsCount;
    @Nullable
    public final Date updatedAt;

    public Repo(long id, String name, String fullName, String description, String homepage, String language, String webUrl, long forksCount, long watchersCount, long starsCount, @Nullable Date updatedAt) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.homepage = homepage;
        this.language = language;
        this.webUrl = webUrl;
        this.forksCount = forksCount;
        this.watchersCount = watchersCount;
        this.starsCount = starsCount;
        this.fullName = fullName;
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Repo[" + fullName + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Repo)) {
            return false;
        }
        Repo other = (Repo) obj;
        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        int result = 1;

        Object[] attrs = new Object[]{name, fullName, description, homepage, language, webUrl, forksCount, watchersCount, starsCount, updatedAt};
        for (Object element : attrs)
            result = 31 * result + (element == null ? 0 : element.hashCode());

        return result;
    }
}
