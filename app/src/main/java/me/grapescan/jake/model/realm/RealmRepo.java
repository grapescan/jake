package me.grapescan.jake.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import me.grapescan.jake.model.Repo;

public class RealmRepo extends RealmObject {
    @PrimaryKey
    private long id;
    private String name;
    private String fullName;
    private String description;
    private String homepage;
    private String language;
    private String webUrl;
    private long forksCount;
    private long watchersCount;
    private long starsCount;
    private long updatedAt;

    public RealmRepo() {}

    void copyFrom(Repo src) {
        name = src.name;
        fullName = src.fullName;
        description = src.description;
        homepage = src.homepage;
        language = src.language;
        webUrl = src.webUrl;
        forksCount = src.forksCount;
        watchersCount = src.watchersCount;
        starsCount = src.starsCount;
        updatedAt = src.updatedAt == null ? -1 : src.updatedAt.getTime();
    }
    
    Repo toRepo() {
        return new Repo(id, name, fullName, description, homepage, language, webUrl, forksCount, watchersCount, starsCount,
                updatedAt == -1 ? null : new Date(updatedAt));
    }
}