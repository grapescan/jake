package me.grapescan.jake.model;


public interface PagedDataSource<T> {
    interface Callback<T> {
        void onDataLoaded(T data);
        void onError(Throwable error);
    }

    void loadNextPage(int offset, Callback<T> callback);
}
