package me.grapescan.jake.model;

public interface LocalCache<T> extends DataSource<T>, DataStorage<T> {
}
