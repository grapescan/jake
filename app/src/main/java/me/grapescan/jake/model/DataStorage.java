package me.grapescan.jake.model;

import android.support.annotation.NonNull;

interface DataStorage<T> {
    void save(@NonNull T data);

    void clear();
}
