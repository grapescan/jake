package me.grapescan.jake.model.realm;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import me.grapescan.jake.model.Repo;

public class RepoListContainer extends RealmObject {
    @PrimaryKey
    private int id;

    private RealmList<RealmRepo> realmRepos;

    public RepoListContainer() {}

    void put(List<Repo> repos) {
        realmRepos = getRealmRepos(repos);
    }

    public List<Repo> get() {
        return getRepos(realmRepos);
    }

    private RealmList<RealmRepo> getRealmRepos(List<Repo> repos) {
        RealmList<RealmRepo> result = new RealmList<>();
        for (Repo repo : repos) {
            result.add(getRealmRepo(repo));
        }
        Realm realm = Realm.getDefaultInstance();
        realm.copyToRealmOrUpdate(result);
        realm.close();
        return result;
    }

    private List<Repo> getRepos(RealmList<RealmRepo> realmRepos) {
        List<Repo> result = new ArrayList<>(realmRepos.size());
        for (RealmRepo realmRepo : realmRepos) {
            result.add(realmRepo.toRepo());
        }
        return result;
    }

    private RealmRepo getRealmRepo(final Repo repo) {
        RealmRepo result = Realm.getDefaultInstance().where(RealmRepo.class).equalTo("id", repo.id).findFirst();
        if (result == null) {
            result = Realm.getDefaultInstance().createObject(RealmRepo.class, repo.id);
        }
        result.copyFrom(repo);
        return result;
    }
}
