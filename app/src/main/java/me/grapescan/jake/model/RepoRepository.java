package me.grapescan.jake.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class RepoRepository {

    public interface Callback {
        void onDataLoaded(List<Repo> data);
        void onError(Throwable error);
    }

    private static final String TAG = "RepoRepository";
    private final LocalCache<List<Repo>> localCache;
    private final PagedDataSource<List<Repo>> remoteSource;
    private AtomicBoolean canLoadMore = new AtomicBoolean(true);

    public RepoRepository(LocalCache<List<Repo>> localCache, PagedDataSource<List<Repo>> remoteSource) {
        this.localCache = localCache;
        this.remoteSource = remoteSource;
    }

    public void getRepos(final Callback callback) {
        localCache.load(new DataSource.Callback<List<Repo>>() {
            @Override
            public void onDataLoaded(List<Repo> data) {
                if (data.isEmpty()) {
                    remoteSource.loadNextPage(0, new PagedDataSource.Callback<List<Repo>>() {
                        @Override
                        public void onDataLoaded(List<Repo> data) {
                            localCache.save(data);
                            canLoadMore.set(!data.isEmpty());
                            callback.onDataLoaded(data);
                        }

                        @Override
                        public void onError(Throwable error) {
                            callback.onError(error);
                        }
                    });
                } else {
                    callback.onDataLoaded(data);
                }
            }

            @Override
            public void onError(Throwable error) {
                callback.onError(error);
            }
        });
    }

    public void getFreshRepos(Callback repositoryCallback) {
        localCache.clear();
        getRepos(repositoryCallback);
    }

    public boolean canLoadMore() {
        return canLoadMore.get();
    }

    public void loadMore(final Callback callback) {
        localCache.load(new DataSource.Callback<List<Repo>>() {
            @Override
            public void onDataLoaded(final List<Repo> localData) {
                Log.d(TAG, "local cache has " + localData.size() + " items");
                remoteSource.loadNextPage(localData.size(), new PagedDataSource.Callback<List<Repo>>() {
                    @Override
                    public void onDataLoaded(List<Repo> data) {
                        Log.d(TAG, "loaded " + data.size() + " new items");
                        Set<Repo> mergedData = new LinkedHashSet<>(localData);
                        mergedData.addAll(data);
                        canLoadMore.set(localData.size() < mergedData.size());
                        List<Repo> result = new ArrayList<>(mergedData);
                        localCache.save(result);
                        callback.onDataLoaded(result);
                    }

                    @Override
                    public void onError(Throwable error) {
                        callback.onError(error);
                    }
                });
            }

            @Override
            public void onError(Throwable error) {
                callback.onError(error);
            }
        });
    }
}
