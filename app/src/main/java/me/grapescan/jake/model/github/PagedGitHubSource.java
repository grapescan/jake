package me.grapescan.jake.model.github;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.grapescan.jake.BuildConfig;
import me.grapescan.jake.model.PagedDataSource;
import me.grapescan.jake.model.Repo;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class PagedGitHubSource implements PagedDataSource<List<Repo>> {

    private interface GitHubApi {
        @GET("users/{id}/repos")
        Call<List<RepoEntity>> getRepos(@Path("id") String userId, @Query("page") int pageNumber, @Query("per_page") int perPage);
    }

    @SuppressLint("SimpleDateFormat")
    private static final DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
    private static final String TAG = "PagedGitHubSource";
    private final GitHubApi api;
    private final int pageSize;

    public PagedGitHubSource(int pageSize) {
        this.pageSize = pageSize;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            httpClient.addInterceptor(logging);
        }
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl url = original.url().newBuilder()
                        .addQueryParameter("client_id", "fb7a7d8cc727c7f1253c")
                        .addQueryParameter("client_secret", "3e627dfa59b023e0d816968ef53ccf53c51bbd09")
                        .addQueryParameter("sort", "updated")
                        .addQueryParameter("direction", "desc")
                        .build();
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);
                return chain.proceed(requestBuilder.build());
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(GitHubApi.class);
    }

    @Override
    public void loadNextPage(final int offset, final Callback<List<Repo>> callback) {
        int firstPageIndex = 1;
        int pageNumber = (offset / pageSize) + firstPageIndex;
        api.getRepos("JakeWharton", pageNumber, pageSize).enqueue(new retrofit2.Callback<List<RepoEntity>>() {
            @Override
            public void onResponse(Call<List<RepoEntity>> call, Response<List<RepoEntity>> response) {
                List<RepoEntity> entities;
                if (response.code() != 200 || response.body() == null) {
                    callback.onError(new Exception("Request failed"));
                    return;
                } else {
                    entities = response.body();
                }
                List<Repo> data = new ArrayList<>(entities.size());
                for (RepoEntity repoEntity : response.body()) {
                    data.add(getRepo(repoEntity));
                }
                callback.onDataLoaded(data);
            }

            @Override
            public void onFailure(Call<List<RepoEntity>> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    private Repo getRepo(RepoEntity entity) {
        return new Repo(
                entity.id,
                entity.name,
                entity.fullName,
                entity.description,
                entity.homepage,
                entity.language,
                entity.webUrl,
                entity.forksCount,
                entity.watchersCount,
                entity.starsCount,
                parseDate(entity.updatedAt));
    }

    @Nullable
    private Date parseDate(String dateString) {
        try {
            return dateTimeFormat.parse(dateString/*.replaceAll("Z$", "+0000")*/);
        } catch (ParseException e) {
            Log.e(TAG, "failed to parse date: " + dateString, e);
            return null;
        }
    }
}
