package me.grapescan.jake.model.github;

import com.google.gson.annotations.SerializedName;

class RepoEntity {
    @SerializedName("id")
    public long id;

    @SerializedName("name")
    public String name;

    @SerializedName("full_name")
    public String fullName;

    @SerializedName("description")
    public String description;

    @SerializedName("html_url")
    public String webUrl;

    @SerializedName("homepage")
    public String homepage;

    @SerializedName("language")
    public String language;

    @SerializedName("forks_count")
    public long forksCount;

    @SerializedName("stargazers_count")
    public long starsCount;

    @SerializedName("watchers_count")
    public long watchersCount;

    @SerializedName("updated_at")
    public String updatedAt;
}
