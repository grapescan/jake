package me.grapescan.jake.model.realm;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import me.grapescan.jake.model.LocalCache;
import me.grapescan.jake.model.Repo;

public class RealmCache implements LocalCache<List<Repo>> {

    public RealmCache(Context context) {
        Realm.init(context);
    }

    @Override
    public void save(@NonNull final List<Repo> data) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RepoListContainer container = realm.where(RepoListContainer.class).findFirst();
                if (container == null) {
                    container = Realm.getDefaultInstance().createObject(RepoListContainer.class, 0);
                }
                container.put(data);
            }
        });
    }

    @Override
    public void clear() {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(RepoListContainer.class);
            }
        });
    }

    @Override
    public void load(Callback<List<Repo>> callback) {
        try {
            RepoListContainer data = Realm.getDefaultInstance().where(RepoListContainer.class).findFirst();
            callback.onDataLoaded(data == null ? Collections.<Repo>emptyList() : data.get());
        } catch (Throwable throwable) {
            callback.onError(throwable);
        }
    }
}
