package me.grapescan.jake;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import me.grapescan.jake.model.RepoRepository;
import me.grapescan.jake.model.github.PagedGitHubSource;
import me.grapescan.jake.model.realm.RealmCache;
import me.grapescan.jake.repos.format.counter.CounterFormatterImpl;
import me.grapescan.jake.repos.format.time.RelativeTimeFormatterImpl;
import me.grapescan.jake.repos.RepoListContract;
import me.grapescan.jake.repos.RepoListMessages;
import me.grapescan.jake.repos.RepoListPresenter;
import me.grapescan.jake.repos.format.time.RelativeTimeMessages;

public class App extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static RepoListContract.Presenter repoListPresenter;
    private static RepoRepository repoRepository;
    private static UiHandler uiHandler = new UiHandler() {
        private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void runOnUiThread(Runnable task) {
            mainThreadHandler.post(task);
        }
    };

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getAppContext() {
        return context;
    }

    public static RepoListContract.Presenter getRepoListPresenter() {
        if (repoListPresenter == null) {
            RepoListMessages messages = new RepoListMessages(context);
            repoListPresenter = new RepoListPresenter(uiHandler, getRepoRepository(), messages,
                    new CounterFormatterImpl(),
                    new RelativeTimeFormatterImpl(new RelativeTimeMessages(context)));
        }
        return repoListPresenter;
    }

    private static RepoRepository getRepoRepository() {
        if (repoRepository == null) {
            repoRepository = new RepoRepository(new RealmCache(context), new PagedGitHubSource(15));
        }
        return repoRepository;
    }
}
