package me.grapescan.jake.repos.format.time;

public interface MessageProvider {
    String seconds(int n);
    String minutes(int n);
    String hours(int n);
    String days(int n);
    String weeks(int n);
    String months(int n);
    String years(int n);
}
