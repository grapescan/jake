package me.grapescan.jake.repos.format.time;

import java.util.Date;


public interface RelativeTimeFormatter {
    String format(Date startDate, Date endDate);
}
