package me.grapescan.jake.repos;

import android.content.Context;

import me.grapescan.jake.R;

public class RepoListMessages implements MessageProvider {
    private Context context;

    public RepoListMessages(Context context) {
        this.context = context;
    }

    @Override
    public String genericLoadingError() {
        return context.getResources().getString(R.string.repos_error_loading_failed);
    }

    @Override
    public String noNetworkError() {
        return context.getResources().getString(R.string.error_network_unavailable);
    }

    @Override
    public String updatedOnTemplate() {
        return context.getResources().getString(R.string.repo_card_updated_on);
    }
}
