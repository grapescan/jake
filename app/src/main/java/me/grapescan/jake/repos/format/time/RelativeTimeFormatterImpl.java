package me.grapescan.jake.repos.format.time;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class RelativeTimeFormatterImpl implements RelativeTimeFormatter {
    private static final long ONE_MINUTE = TimeUnit.MINUTES.toMillis(1);
    private static final long ONE_HOUR = TimeUnit.HOURS.toMillis(1);
    private static final long ONE_DAY = TimeUnit.DAYS.toMillis(1);
    private static final long ONE_WEEK = TimeUnit.DAYS.toMillis(7);
    private MessageProvider messages;

    public RelativeTimeFormatterImpl(MessageProvider messages) {
        this.messages = messages;
    }

    @Override
    public String format(Date startDate, Date endDate) {
        long interval = Math.abs(startDate.getTime() - endDate.getTime());

        if (interval < ONE_MINUTE) {
            long seconds = TimeUnit.MILLISECONDS.toSeconds(interval);
            return messages.seconds((int) seconds);
        }

        if (interval < ONE_HOUR) {
            long minutes = TimeUnit.MILLISECONDS.toMinutes(interval);
            return messages.minutes((int) minutes);
        }

        if (interval < ONE_DAY) {
            long hours = TimeUnit.MILLISECONDS.toHours(interval);
            return messages.hours((int) hours);
        }

        if (interval < ONE_WEEK) {
            long days = TimeUnit.MILLISECONDS.toDays(interval);
            return messages.days((int) days);
        }

        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(new Date(Math.min(startDate.getTime(), endDate.getTime())));
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(new Date(Math.max(startDate.getTime(), endDate.getTime())));

        int years = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        long weeks = TimeUnit.MILLISECONDS.toDays(interval) / 7;

        if (years > 0) {
            return messages.years(years);
        } else if (weeks >= 4) {
            return messages.months((int) (weeks / 4));
        } else {
            return messages.weeks((int) weeks);
        }
    }
}
