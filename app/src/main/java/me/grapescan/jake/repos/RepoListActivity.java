package me.grapescan.jake.repos;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import me.grapescan.jake.App;
import me.grapescan.jake.BaseCard;
import me.grapescan.jake.R;
import me.grapescan.jake.repos.list.RepoClickListener;
import me.grapescan.jake.repos.list.RepoListAdapter;

public class RepoListActivity extends AppCompatActivity implements RepoListContract.View {
    protected AppBarLayout appbar;
    protected Toolbar toolbar;
    protected ProgressBar progressBar;
    protected SwipeRefreshLayout swipeLayout;
    protected RecyclerView list;

    private RepoListAdapter adapter;

    private RepoListContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_list);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onRefresh();
            }
        });
        list = (RecyclerView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        initToolbar();
        initList();

        presenter = App.getRepoListPresenter();
    }

    private void initToolbar() {
        toolbar.setTitle(R.string.title);
        toolbar.setSubtitle(R.string.subtitle);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onAttach(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onDetach();
    }

    private void initList() {
        adapter = new RepoListAdapter(new RepoClickListener() {
            @Override
            public void onRepoClick(long id) {
                presenter.onRepoClick(id);
            }
        });
        list.setHasFixedSize(true);
        list.setAdapter(adapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                presenter.onItemDisplayed(layoutManager.findLastVisibleItemPosition());
            }
        });
        list.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void stopRefreshing() {
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        list.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        list.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showList(List<BaseCard> cards) {
        adapter.setItems(cards);
        hideProgress();
    }

    @Override
    public void openUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}
