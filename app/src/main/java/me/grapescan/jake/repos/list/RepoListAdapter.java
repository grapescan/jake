package me.grapescan.jake.repos.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.grapescan.jake.BaseCard;
import me.grapescan.jake.BaseViewHolder;
import me.grapescan.jake.R;

public class RepoListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private final RepoClickListener repoClickListener;
    private List<BaseCard> items = new ArrayList<>();

    public RepoListAdapter(RepoClickListener repoClickListener) {

        this.repoClickListener = repoClickListener;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo, parent, false);
                return new RepoViewHolder(view, repoClickListener);
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
                return new ProgressViewHolder(view);
            default:
                throw new IllegalStateException("Unknown viewType: " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<BaseCard> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
