package me.grapescan.jake.repos.format.time;

import android.content.Context;

import me.grapescan.jake.R;

public class RelativeTimeMessages implements MessageProvider {

    private Context context;

    public RelativeTimeMessages(Context context) {
        this.context = context;
    }

    @Override
    public String seconds(int n) {
        return context.getResources().getQuantityString(R.plurals.seconds, n, n);
    }

    @Override
    public String minutes(int n) {
        return context.getResources().getQuantityString(R.plurals.minutes, n, n);
    }

    @Override
    public String hours(int n) {
        return context.getResources().getQuantityString(R.plurals.hours, n, n);
    }

    @Override
    public String days(int n) {
        return context.getResources().getQuantityString(R.plurals.days, n, n);
    }

    @Override
    public String weeks(int n) {
        return context.getResources().getQuantityString(R.plurals.weeks, n, n);
    }

    @Override
    public String months(int n) {
        return context.getResources().getQuantityString(R.plurals.months, n, n);
    }

    @Override
    public String years(int n) {
        return context.getResources().getQuantityString(R.plurals.years, n, n);
    }
}
