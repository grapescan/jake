package me.grapescan.jake.repos.list;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import me.grapescan.jake.BaseViewHolder;
import me.grapescan.jake.R;

class RepoViewHolder extends BaseViewHolder<RepoCard> {
    private TextView title;
    private TextView subtitle;
    private TextView description;
    private TextView starsCount;
    private TextView forksCount;
    private TextView language;
    private final RepoClickListener clickListener;

    RepoViewHolder(View view, RepoClickListener clickListener) {
        super(view);
        title = (TextView) view.findViewById(R.id.repo_title);
        subtitle = (TextView) view.findViewById(R.id.repo_subtitle);
        description = (TextView) view.findViewById(R.id.repo_description);
        starsCount= (TextView) view.findViewById(R.id.repo_stars_count);
        forksCount = (TextView) view.findViewById(R.id.repo_forks_count);
        language = (TextView) view.findViewById(R.id.repo_language);
        this.clickListener = clickListener;
    }

    public void bind(final RepoCard card) {
        title.setText(card.title);
        subtitle.setText(card.updatedOn);
        subtitle.setVisibility(TextUtils.isEmpty(card.updatedOn) ? View.GONE : View.VISIBLE);
        description.setText(card.description);
        starsCount.setText(card.starsCount);
        forksCount.setText(card.forksCount);
        language.setText(card.language);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onRepoClick(card.id);
            }
        });
    }
}
