package me.grapescan.jake.repos;

import java.util.List;

import me.grapescan.jake.BaseCard;

public interface RepoListContract {
    interface View {
        void showList(List<BaseCard> cards);

        void showProgress();

        void hideProgress();

        void stopRefreshing();

        void showError(String message);

        void openUrl(String url);
    }

    interface Presenter {
        void onAttach(View view);

        void onDetach();

        void onRefresh();

        void onItemDisplayed(int position);

        void onRepoClick(long repoId);
    }
}
