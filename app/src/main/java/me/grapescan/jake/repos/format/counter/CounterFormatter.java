package me.grapescan.jake.repos.format.counter;

public interface CounterFormatter {
    String format(long count);
}
