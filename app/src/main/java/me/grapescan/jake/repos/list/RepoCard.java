package me.grapescan.jake.repos.list;

import me.grapescan.jake.BaseCard;

public class RepoCard implements BaseCard {
    public final long id;
    public final String title;
    public final String description;
    public final String updatedOn;
    public final String starsCount;
    public final String forksCount;
    public final String language;

    public RepoCard(long id, String title, String description, String updatedOn, String starsCount, String forksCount, String language) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.updatedOn = updatedOn;
        this.starsCount = starsCount;
        this.forksCount = forksCount;
        this.language = language;
    }

    @Override
    public int getViewType() {
        return 0;
    }
}
