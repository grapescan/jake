package me.grapescan.jake.repos;

import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import me.grapescan.jake.BaseCard;
import me.grapescan.jake.UiHandler;
import me.grapescan.jake.model.Repo;
import me.grapescan.jake.model.RepoRepository;
import me.grapescan.jake.repos.format.counter.CounterFormatter;
import me.grapescan.jake.repos.format.time.RelativeTimeFormatter;
import me.grapescan.jake.repos.list.ProgressCard;
import me.grapescan.jake.repos.list.RepoCard;

public class RepoListPresenter implements RepoListContract.Presenter {

    interface ViewAction {
        void execute(@NonNull RepoListContract.View view);
    }

    private static final String TAG = "RepoListPresenter";
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    private final UiHandler uiHandler;
    private final CounterFormatter counterFormatter;
    private final RelativeTimeFormatter relativeTimeFormatter;
    private WeakReference<RepoListContract.View> viewRef = new WeakReference<>(null);
    private RepoRepository repository;
    private MessageProvider messages;

    private List<Repo> data;
    private AtomicBoolean isLoadingMore = new AtomicBoolean(false);

    private String getErrorMessage(Throwable error) {
        if (error instanceof UnknownHostException) {
            return messages.noNetworkError();
        }
        Log.e(TAG, "unknown error: " + error.getMessage(), error);
        return messages.genericLoadingError();
    }

    public RepoListPresenter(UiHandler uiHandler, RepoRepository repository, MessageProvider messages, CounterFormatter counterFormatter, RelativeTimeFormatter relativeTimeFormatter) {
        this.uiHandler = uiHandler;
        this.repository = repository;
        this.messages = messages;
        this.counterFormatter = counterFormatter;
        this.relativeTimeFormatter = relativeTimeFormatter;
    }

    @Override
    public void onAttach(RepoListContract.View view) {
        viewRef = new WeakReference<>(view);
        if (data == null) {
            showProgress();
            executorService.submit(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    repository.getRepos(new RepoRepository.Callback() {
                        @Override
                        public void onDataLoaded(final List<Repo> data) {
                            RepoListPresenter.this.data = data;
                            updateList();
                        }

                        @Override
                        public void onError(final Throwable error) {
                            Log.e(TAG, "loading error", error);
                            hideProgress();
                            showPopupError(error);
                        }
                    });
                    return null;
                }
            });
        } else {
            updateList();
        }
    }

    private void hideProgress() {
        performViewAction(new ViewAction() {
            @Override
            public void execute(@NonNull RepoListContract.View view) {
                view.hideProgress();
            }
        });
    }

    private void showProgress() {
        performViewAction(new ViewAction() {
            @Override
            public void execute(@NonNull RepoListContract.View view) {
                view.showProgress();
            }
        });
    }

    @Override
    public void onDetach() {

    }

    @Override
    public void onRefresh() {
        executorService.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                repository.getFreshRepos(new RepoRepository.Callback() {
                    @Override
                    public void onDataLoaded(final List<Repo> data) {
                        RepoListPresenter.this.data = data;
                        updateList();
                    }

                    @Override
                    public void onError(final Throwable error) {
                        showPopupError(error);
                    }
                });
                return null;
            }
        });
    }

    private void showPopupError(final Throwable error) {
        performViewAction(new ViewAction() {
            @Override
            public void execute(@NonNull RepoListContract.View view) {
                view.stopRefreshing();
                view.showError(getErrorMessage(error));
            }
        });
    }

    private void updateList() {
        performViewAction(new ViewAction() {
            @Override
            public void execute(@NonNull RepoListContract.View view) {
                view.stopRefreshing();
                view.showList(getCards(data, repository.canLoadMore()));
            }
        });
    }

    private void performViewAction(final ViewAction viewAction) {
        uiHandler.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RepoListContract.View view = viewRef.get();
                if (view != null) {
                    viewAction.execute(view);
                }
            }
        });
    }

    private List<BaseCard> getCards(List<Repo> data, boolean showLoadingMore) {
        List<BaseCard> cards = new ArrayList<>(data.size());
        for (Repo repo : data) {
            cards.add(new RepoCard(repo.id,
                    repo.name,
                    repo.description,
                    relativeTimeFormatter.format(repo.updatedAt, new Date()),
                    counterFormatter.format(repo.starsCount),
                    counterFormatter.format(repo.forksCount),
                    repo.language));
        }
        if (showLoadingMore) {
            cards.add(new ProgressCard());
        }
        return cards;
    }

    @Override
    public void onItemDisplayed(int position) {
        if (data != null && data.size() - position < 3 && repository.canLoadMore() && !isLoadingMore.get()) {
            isLoadingMore.set(true);
            repository.loadMore(new RepoRepository.Callback() {
                @Override
                public void onDataLoaded(final List<Repo> data) {
                    isLoadingMore.set(false);
                    RepoListPresenter.this.data = data;
                    updateList();
                }

                @Override
                public void onError(final Throwable error) {
                    isLoadingMore.set(false);
                    hideProgress();
                    updateList();
                    showPopupError(error);
                }
            });
        }
    }

    @Override
    public void onRepoClick(long repoId) {
        for (final Repo repo : data) {
            if (repo.id == repoId) {
                performViewAction(new ViewAction() {
                    @Override
                    public void execute(@NonNull RepoListContract.View view) {
                        view.openUrl(repo.webUrl);
                    }
                });
                break;
            }
        }
    }
}
