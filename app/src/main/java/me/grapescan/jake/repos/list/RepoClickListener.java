package me.grapescan.jake.repos.list;

public interface RepoClickListener {
    void onRepoClick(long id);
}