package me.grapescan.jake.repos;

public interface MessageProvider {
    String genericLoadingError();
    String noNetworkError();
    String updatedOnTemplate();
}
