package me.grapescan.jake.repos.list;

import android.view.View;

import me.grapescan.jake.BaseViewHolder;

class ProgressViewHolder extends BaseViewHolder<ProgressCard> {

    ProgressViewHolder(View view) {
        super(view);
    }

    public void bind(final ProgressCard card) {
    }
}
